"""
Module description
"""

from fastapi import FastAPI

app = FastAPI()


@app.get('/')
def index():
    """
    Return version of the service
    """
    return {'version': 'latest'}


def add_function(value1, value2):
    """
    Adding 2 numbers
    """
    return value1+value2
