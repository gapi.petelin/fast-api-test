# Lightweight python image
FROM python:3.9-alpine
# Copy source code into the image
COPY . /app
WORKDIR /app
# Instal requirements
RUN pip install -r requirements.txt
# Expose port 
EXPOSE 80
# Start the service
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
